import InfiniteScroller from "./components/InfiniteScroller";
import './App.css';

function App() {
  return (
    <div className="App">
      <InfiniteScroller />
    </div>
  );
}

export default App;
