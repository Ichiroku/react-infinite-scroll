import React, { useState, useEffect, useRef } from 'react'

export default function InfiniteScroller() {
    const [items, setItems] = useState([1,2,3,4]);
    const [page, setPage] = useState(0);
    const loader = useRef(null);

    const handleObserver = (entities) => {
        const target = entities[0];
        if (target.isIntersecting) {   
            alert("Alcanzado fin de scroll");
            setPage((page) => page + 1)
        }
    }

    useEffect(() => {
        const options = { root: null, rootMargin: "20px", threshold: 1.0 };
        const observer = new IntersectionObserver(handleObserver, options);
        if (loader.current) {
           observer.observe(loader.current)
        }
   }, []);

   useEffect(() => {
        setTimeout(function() {
            //Simula tiempo de espera por respuesta de API
            const newList = [...items, 5,6,7,8];
            setItems(newList);
        }, 2000);    

    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [page])

    const renderItems= () => {
        return items.map((item, index) => {
            return (
                <div key={index} className="Scroller-item">
                    <h2> {item} </h2>
                </div>
            )
        })
    }

    return (
        <div className="Scroller-container">
            <div className="Scroller-list">
                { renderItems() }
                <div className="loading" ref={loader}>
                    <h2>Load More</h2>
                </div>
            </div>
        </div>
    )
}
